package studentdashboards.service;

import java.util.Date;
import java.util.List;

import studentdashboards.model.Data;
import studentdashboards.model.Data2D;
import studentdashboards.model.Data3D;

public interface DataDao {

	public List<Data> getByUser(String userId);

	public List<Data2D<Date, Integer>> getSummaryByEvents();

	public List<Data2D<Date, Integer>> getSummaryByEvents(String userId);

	public List<Data2D<Date, Integer>> getSummaryByNumOfUsers();

	public List<Data2D<Date, Integer>> getSummaryByWeekday();

	public List<Data2D<Date, Integer>> getSummaryByWeekday(String userId);

	public List<Data2D<Date, Integer>> getSummaryByTime();

	public List<Data2D<Date, Integer>> getSummaryByTime(String userId);

	public List<Data3D<String, Date, Integer>> getDetailByAction();

	public List<Data3D<String, Date, Integer>> getDetailByAction(String userId);

	public List<Data3D<String, Date, Integer>> getDetailByAction(int week);

	public List<Data3D<String, Date, Integer>> getDetailByAction(String userId, int week);

	public List<Data> getExco();

	public List<Data> getExco(String userId);

	public List<Data> getEmbeddedVideo();

	public List<Data> getEmbeddedVideo(String userId);

}
