package studentdashboards.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.jdbc.core.RowMapper;

import studentdashboards.model.Data3D;

public class Data3DMapper implements RowMapper<Data3D<String, Date, Integer>> {

	public Data3D<String, Date, Integer> mapRow(ResultSet rs, int rowNum) throws SQLException {
		Data3D<String, Date, Integer> data = new Data3D<String, Date, Integer>();
		data.setStream(rs.getString("stream"));
		data.setX(rs.getTimestamp("received"));
		data.setY(rs.getInt("num"));
		return data;
	}

}
