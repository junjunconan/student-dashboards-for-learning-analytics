package studentdashboards.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.jdbc.core.RowMapper;

import studentdashboards.model.Data2D;

public class Data2DMapper implements RowMapper<Data2D<Date, Integer>> {

	public Data2D<Date, Integer> mapRow(ResultSet rs, int rowNum) throws SQLException {
		Data2D<Date, Integer> data = new Data2D<Date, Integer>();
		data.setX(rs.getTimestamp("received"));
		data.setY(rs.getInt("num"));
		return data;
	}

}
