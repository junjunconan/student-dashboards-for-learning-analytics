package studentdashboards.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import studentdashboards.model.Data;
import studentdashboards.model.Data2D;
import studentdashboards.model.Data3D;

@Repository
public class DataDaoImpl implements DataDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public List<Data> getByUser(String userId) {
		String SQL = "SELECT * FROM data WHERE user_id='" + userId + "'";
		List<Data> result = jdbcTemplate.query(SQL, new DataMapper());
		return result;
	}

	public List<Data2D<Date, Integer>> getSummaryByEvents() {
		String SQL = "SELECT received, count(*) as num FROM data GROUP BY DATE_FORMAT(received, '%Y-%m-%d')";
		List<Data2D<Date, Integer>> result = jdbcTemplate.query(SQL, new Data2DMapper());
		return result;
	}

	public List<Data2D<Date, Integer>> getSummaryByEvents(String userId) {
		String SQL = "SELECT received, count(*) as num FROM data WHERE user_id='" +
				userId + "' GROUP BY DATE_FORMAT(received, '%Y-%m-%d')";
		List<Data2D<Date, Integer>> result = jdbcTemplate.query(SQL, new Data2DMapper());
		return result;
	}

	public List<Data2D<Date, Integer>> getSummaryByNumOfUsers() {
		String SQL = "SELECT received, count(DISTINCT user_id) as num FROM data GROUP BY DATE_FORMAT(received, '%Y-%m-%d')";
		List<Data2D<Date, Integer>> result = jdbcTemplate.query(SQL, new Data2DMapper());
		return result;
	}

	public List<Data2D<Date, Integer>> getSummaryByWeekday() {
		String SQL = "SELECT received, count(*) as num FROM data GROUP BY DAYOFWEEK(received)";
		List<Data2D<Date, Integer>> result = jdbcTemplate.query(SQL, new Data2DMapper());
		return result;
	}

	public List<Data2D<Date, Integer>> getSummaryByWeekday(String userId) {
		String SQL = "SELECT received, count(*) as num FROM data WHERE user_id='" +
				userId + "' GROUP BY DAYOFWEEK(received)";
		List<Data2D<Date, Integer>> result = jdbcTemplate.query(SQL, new Data2DMapper());
		return result;
	}

	public List<Data2D<Date, Integer>> getSummaryByTime() {
		String SQL = "SELECT received, count(*) as num FROM data GROUP BY HOUR(received)";
		List<Data2D<Date, Integer>> result = jdbcTemplate.query(SQL, new Data2DMapper());
		return result;
	}

	public List<Data2D<Date, Integer>> getSummaryByTime(String userId) {
		String SQL = "SELECT received, count(*) as num FROM data WHERE user_id='" +
				userId + "' GROUP BY HOUR(received)";
		List<Data2D<Date, Integer>> result = jdbcTemplate.query(SQL, new Data2DMapper());
		return result;
	}

	public List<Data3D<String, Date, Integer>> getDetailByAction() {
		String SQL = "SELECT action_id as stream, DATE_FORMAT(received, '%Y-%m-%d') as received, count(*) as num FROM data GROUP BY action_id, DATE_FORMAT(received, '%Y-%m-%d')";
		List<Data3D<String, Date, Integer>> result = jdbcTemplate.query(SQL, new Data3DMapper());
		return result;
	}

	public List<Data3D<String, Date, Integer>> getDetailByAction(String userId) {
		String SQL = "SELECT action_id as stream, DATE_FORMAT(received, '%Y-%m-%d') as received, count(*) as num FROM data WHERE user_id='" +
				userId + "' GROUP BY action_id, DATE_FORMAT(received, '%Y-%m-%d')";
		List<Data3D<String, Date, Integer>> result = jdbcTemplate.query(SQL, new Data3DMapper());
		return result;
	}

	public List<Data3D<String, Date, Integer>> getDetailByAction(int week) {
		String SQL = "SELECT action_id as stream, DATE_FORMAT(received, '%Y-%m-%d') as received, count(*) as num FROM data WHERE week="
				+ week + " GROUP BY action_id, DATE_FORMAT(received, '%Y-%m-%d')";
		List<Data3D<String, Date, Integer>> result = jdbcTemplate.query(SQL, new Data3DMapper());
		return result;
	}

	public List<Data3D<String, Date, Integer>> getDetailByAction(String userId, int week) {
		String SQL = "SELECT action_id as stream, DATE_FORMAT(received, '%Y-%m-%d') as received, count(*) as num FROM data WHERE user_id='" +
				userId + "' AND week=" + week + " GROUP BY action_id, DATE_FORMAT(received, '%Y-%m-%d')";
		List<Data3D<String, Date, Integer>> result = jdbcTemplate.query(SQL, new Data3DMapper());
		return result;
	}

	public List<Data> getExco() {
		String SQL = "SELECT * FROM data WHERE action_id='exco-answer'";
		List<Data> result = jdbcTemplate.query(SQL, new DataMapper());
		return result;
	}


	public List<Data> getExco(String userId) {
		String SQL = "SELECT * FROM data WHERE user_id='" + userId + "' and action_id='exco-answer'";
		List<Data> result = jdbcTemplate.query(SQL, new DataMapper());
		return result;
	}

	public List<Data> getEmbeddedVideo() {
		String SQL = "SELECT * FROM data WHERE action_id='embedded-video' ORDER BY week ASC";
		List<Data> result = jdbcTemplate.query(SQL, new DataMapper());
		return result;
	}


	public List<Data> getEmbeddedVideo(String userId) {
		String SQL = "SELECT * FROM data WHERE user_id='" + userId + "' and action_id='embedded-video' ORDER By week ASC";
		List<Data> result = jdbcTemplate.query(SQL, new DataMapper());
		return result;
	}

}
