package studentdashboards.service;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import studentdashboards.model.Data;

public class DataMapper implements RowMapper<Data> {

	public Data mapRow(ResultSet rs, int rowNum) throws SQLException {
		Data data = new Data();
		data.setId(rs.getInt("id"));
		data.setReceived(rs.getTimestamp("received"));
		data.setUserId(rs.getString("user_id"));
		data.setActionId(rs.getString("action_id"));
		data.setContext(rs.getString("context"));
		data.setPayload(rs.getString("payload"));
		data.setRemoteAddr(rs.getString("remote_addr"));
		data.setWeek(rs.getInt("week"));
		data.setTopic(rs.getString("topic"));
		return data;
	}

}
