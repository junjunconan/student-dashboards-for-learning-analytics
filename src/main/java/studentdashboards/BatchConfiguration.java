package studentdashboards;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;

import studentdashboards.model.ImportData;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

	private static final int CHUNK_SIZE = 10000;

	@Bean
	public ItemReader<ImportData> reader() {
		FlatFileItemReader<ImportData> reader = new FlatFileItemReader<ImportData>();
		reader.setResource(new FileSystemResource("data.csv"));
		reader.setLinesToSkip(1);
		reader.setLineMapper(new DefaultLineMapper<ImportData>() {{
			setLineTokenizer(new DelimitedLineTokenizer() {{
				setNames(new String[] { "id", "received", "context", "userId", "actionId", "payload", "remoteAddr",
						"week", "topic" });
			}});
			setFieldSetMapper(new BeanWrapperFieldSetMapper<ImportData>() {{
				setTargetType(ImportData.class);
			}});
		}});
		return reader;
	}

	@Bean
	public ItemWriter<ImportData> writer(DataSource dataSource) {
		JdbcBatchItemWriter<ImportData> writer = new JdbcBatchItemWriter<ImportData>();
		writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<ImportData>());
		writer.setSql("INSERT INTO data (id, received, context, user_id, action_id, payload, remote_addr, week, topic)"
				+ " VALUES (:id, :received, :context, :userId, :actionId, :payload, :remoteAddr, :week, :topic)");
		writer.setDataSource(dataSource);
		return writer;
	}

	@Bean
	public ItemProcessor<ImportData, ImportData> processor() {
		return new DataItemProcessor();
	}

	@Bean
	public Job importJob(JobBuilderFactory jobs, Step s1, JobExecutionListener listener) {
		return jobs.get("importJob")
				.incrementer(new RunIdIncrementer())
				.listener(listener)
				.flow(s1)
				.end()
				.build();
	}

	@Bean
	public Step step1(StepBuilderFactory stepBuilderFactory, ItemReader<ImportData> reader,
			ItemWriter<ImportData> writer, ItemProcessor<ImportData, ImportData> processor) {
		return stepBuilderFactory.get("step1")
				.<ImportData, ImportData> chunk(CHUNK_SIZE)
				.reader(reader)
				.processor(processor)
				.writer(writer)
				.build();
	}

}
