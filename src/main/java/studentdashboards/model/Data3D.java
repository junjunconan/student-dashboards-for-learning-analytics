package studentdashboards.model;

public class Data3D<T1, T2, T3> {

	private T1 stream;
	private T2 x;
	private T3 y;

	public Data3D() {}

	public Data3D(T1 stream, T2 x, T3 y) {
		this.stream = stream;
		this.x = x;
		this.y = y;
	}

	public T1 getStream() {
		return stream;
	}

	public void setStream(T1 stream) {
		this.stream = stream;
	}

	public T2 getX() {
		return x;
	}

	public void setX(T2 x) {
		this.x = x;
	}

	public T3 getY() {
		return y;
	}

	public void setY(T3 y) {
		this.y = y;
	}

}
