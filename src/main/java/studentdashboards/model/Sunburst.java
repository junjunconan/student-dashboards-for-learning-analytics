package studentdashboards.model;

import java.util.List;

public class Sunburst implements SunburstNode {

	private String name;
	private List<SunburstNode> children;

	public Sunburst(String name, List<SunburstNode> children) {
		super();
		this.name = name;
		this.children = children;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<SunburstNode> getChildren() {
		return children;
	}

	public void setChildren(List<SunburstNode> children) {
		this.children = children;
	}

}
