package studentdashboards.model;

public class ImportData {

	private String id;
	private String received;
	private String context;
	private String userId;
	private String actionId;
	private String payload;
	private String remoteAddr;
	private String week;
	private String topic;

	public ImportData() {

	}

	public ImportData(String id, String received, String context, String userId,
			String actionId, String payload, String remoteAddr, String week,
			String topic) {
		this.id = id;
		this.received = received;
		this.context = context;
		this.userId = userId;
		this.actionId = actionId;
		this.payload = payload;
		this.remoteAddr = remoteAddr;
		this.week = week;
		this.topic = topic;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getReceived() {
		return received;
	}

	public void setReceived(String received) {
		this.received = received;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getActionId() {
		return actionId;
	}

	public void setActionId(String actionId) {
		this.actionId = actionId;
	}

	public String getPayload() {
		return payload;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}

	public String getRemoteAddr() {
		return remoteAddr;
	}

	public void setRemoteAddr(String remoteAddr) {
		this.remoteAddr = remoteAddr;
	}

	public String getWeek() {
		return week;
	}

	public void setWeek(String week) {
		this.week = week;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	@Override
	public String toString() {
		return "Data [id=" + id + ", received=" + received + ", context="
				+ context + ", userId=" + userId + ", actionId=" + actionId
				+ ", payload=" + payload + ", remoteAddr=" + remoteAddr
				+ ", week=" + week + ", topic=" + topic + "]";
	}

}
