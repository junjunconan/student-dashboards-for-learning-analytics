package studentdashboards.model;

public class PieChartModel<T> {

	private String label;
	private T value;

	public PieChartModel() {}

	public PieChartModel(String label, T value) {
		this.label = label;
		this.value = value;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

}
