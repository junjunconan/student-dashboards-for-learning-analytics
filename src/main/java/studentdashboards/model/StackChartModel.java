package studentdashboards.model;

public class StackChartModel<T> {

	private String key;
	private String color;
	private T values;

	public StackChartModel() {}

	public StackChartModel(String key, T values) {
		this.key = key;
		this.values = values;
	}

	public StackChartModel(String key, String color, T values) {
		this.key = key;
		this.color = color;
		this.values = values;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public T getValues() {
		return values;
	}

	public void setValues(T values) {
		this.values = values;
	}

}
