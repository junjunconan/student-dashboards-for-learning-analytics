package studentdashboards.controlller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import studentdashboards.model.Data;
import studentdashboards.model.Data2D;
import studentdashboards.model.StackChartModel;
import studentdashboards.service.DataDao;

@Controller
@Scope("session")
public class ExcoController {

	@Autowired
	private DataDao dataDao;

	@RequestMapping(value = "/exco")
	public String exco(HttpSession session) {
		String user = (String) session.getAttribute("user");
		if (user == null)
			return "redirect:/";
		else
			return "exco";
	}

	@RequestMapping(value = "/exco/get")
	@ResponseBody
	public List<StackChartModel<List<Data2D<String, Integer>>>> getExco(HttpSession session) {
		String user = (String) session.getAttribute("user");
		List<Data> result = null;
		if (user == null)
			return null;
		else if (user.equals("admin"))
			result = dataDao.getExco();
		else
			result = dataDao.getExco(user);

		List<StackChartModel<List<Data2D<String, Integer>>>> chart = new ArrayList<StackChartModel<List<Data2D<String, Integer>>>>();
		Map<String, Integer> incorrect = new HashMap<String, Integer>();
		Map<String, Integer> correct = new HashMap<String, Integer>();

		for (Data data : result) {
			if (data.getPayload().contains("\"incorrect\"")) {
				if (!incorrect.containsKey(data.getContext())) {
					incorrect.put(data.getContext(), -1);
					if (!correct.containsKey(data.getContext()))
						correct.put(data.getContext(), 0);
				}
				else
					incorrect.put(data.getContext(), incorrect.get(data.getContext())-1);
			}
			if (data.getPayload().contains("\"correct\"")) {
				if (!correct.containsKey(data.getContext())) {
					correct.put(data.getContext(), 1);
					if (!incorrect.containsKey(data.getContext()))
						incorrect.put(data.getContext(), 0);
				}
				else
					correct.put(data.getContext(), correct.get(data.getContext())+1);
			}
		}

		List<Data2D<String, Integer>> values = new ArrayList<Data2D<String, Integer>>();
		for (String key : incorrect.keySet()) {
			values.add(new Data2D<String, Integer>(key, incorrect.get(key)));
		}
		chart.add(new StackChartModel<List<Data2D<String, Integer>>>("Incorrect", "#d62728", values));
		values = new ArrayList<Data2D<String, Integer>>();
		for (String key : correct.keySet()) {
			values.add(new Data2D<String, Integer>(key, correct.get(key)));
		}
		chart.add(new StackChartModel<List<Data2D<String, Integer>>>("Correct", "#1f77b4", values));
		return chart;
	}

}
