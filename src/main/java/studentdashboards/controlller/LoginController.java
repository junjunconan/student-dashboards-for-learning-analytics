package studentdashboards.controlller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import studentdashboards.model.Data;
import studentdashboards.service.DataDao;

@Controller
@Scope("session")
public class LoginController {

	@Autowired
	private DataDao dataDao;

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(HttpSession session,
			@RequestParam(value="userid", required=true) String userId) {
		String user = (String) session.getAttribute("user");
		if (user != null && user != "")
			return "redirect:/";
		session.setAttribute("user", userId);
		return "redirect:/";
	}

	@RequestMapping(value = "/login/verify", method = RequestMethod.POST)
	@ResponseBody
	public String verify(@RequestParam(value="userid", required=true) String userId) {
		String success = "{ \"valid\" : true }";
		String fail = "{ \"valid\" : false }";
		if (userId.equals("admin"))
			return success;
		List<Data> result = dataDao.getByUser(userId);
		if (result.size() > 0)
			return success;
		else
			return fail;
	}

	@RequestMapping(value = "/logout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "redirect:/";
	}

}
