package studentdashboards.controlller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import studentdashboards.model.Data2D;
import studentdashboards.model.Data3D;
import studentdashboards.model.StackChartModel;
import studentdashboards.service.DataDao;

@Controller
@Scope("session")
public class DetailController {

	@Autowired
	private DataDao dataDao;

	@RequestMapping(value = "/detail")
	public String detail(HttpSession session) {
		String user = (String) session.getAttribute("user");
		if (user == null)
			return "redirect:/";
		else
			return "detail";
	}

	@RequestMapping(value = "/detail/actions/{week}")
	@ResponseBody
	public List<StackChartModel<List<Data2D<Long, Integer>>>> detailActions(@PathVariable String week,
			HttpSession session) {
		String user = (String) session.getAttribute("user");
		List<Data3D<String, Date, Integer>> result = null;
		if (user == null)
			return null;
		else if (user.equals("admin")) {
			if (week.equals("all"))
				result = dataDao.getDetailByAction();
			else
				result = dataDao.getDetailByAction(Integer.parseInt(week));
		}
		else {
			if (week.equals("all"))
				result = dataDao.getDetailByAction(user);
			else
				result = dataDao.getDetailByAction(user, Integer.parseInt(week));
		}

		List<Data2D<Long, Integer>> values = null;
		List<StackChartModel<List<Data2D<Long, Integer>>>> chart = new ArrayList<StackChartModel<List<Data2D<Long, Integer>>>>();
		Set<Date> dateSet = new TreeSet<Date>();
		Map<String, Map<Date, Integer>> map = new HashMap<String, Map<Date, Integer>>();
		for (Data3D<String, Date, Integer> data : result) {
			if(!dateSet.contains(data.getX()))
				dateSet.add(data.getX());
			if (!map.containsKey(data.getStream()))
				map.put(data.getStream(), new HashMap<Date, Integer>());
			map.get(data.getStream()).put(data.getX(), data.getY());
		}

		for (String stream : map.keySet()) {
			values = new ArrayList<Data2D<Long, Integer>>();
			for (Date date : dateSet) {
				if (map.get(stream).containsKey(date))
					values.add(new Data2D<Long, Integer>(date.getTime(), map.get(stream).get(date)));
				else
					values.add(new Data2D<Long, Integer>(date.getTime(), 0));
			}
			chart.add(new StackChartModel<List<Data2D<Long, Integer>>>(stream, values));
		}

		return chart;
	}

}
