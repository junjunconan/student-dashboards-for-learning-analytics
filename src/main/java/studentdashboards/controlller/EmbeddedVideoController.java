package studentdashboards.controlller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import studentdashboards.model.Data;
import studentdashboards.model.Sunburst;
import studentdashboards.model.SunburstLeaf;
import studentdashboards.model.SunburstNode;
import studentdashboards.service.DataDao;
import studentdashboards.util.YouTubeData;

@Controller
@Scope("session")
public class EmbeddedVideoController {

	@Autowired
	private DataDao dataDao;

	@RequestMapping(value = "/embedded-video")
	public String embeddedVideo(HttpSession session) {
		String user = (String) session.getAttribute("user");
		if (user == null)
			return "redirect:/";
		else
			return "video";
	}

	@RequestMapping(value = "/embedded-video/get")
	@ResponseBody
	public Sunburst getEmbeddedVideo(HttpSession session) throws Exception {
		String user = (String) session.getAttribute("user");
		List<Data> result = null;
		if (user == null)
			return null;
		else if (user.equals("admin"))
			result = dataDao.getEmbeddedVideo();
		else
			result = dataDao.getEmbeddedVideo(user);

		List<SunburstNode> content = new ArrayList<SunburstNode>();
		Map<String, String> titles = new HashMap<String, String>();
		Map<String, Integer> videos = null;
		Sunburst sunburst = null;
		int week = -1;
		for (Data data : result) {
			if (data.getWeek() != week) {
				if (videos != null && videos.keySet().size() > 0) {
					for (String id : videos.keySet()) {
						SunburstNode node = new SunburstLeaf(titles.get(id), videos.get(id));
						sunburst.getChildren().add(node);
					}
				}
				if (sunburst != null) {
					content.add(sunburst);
				}
				week = data.getWeek();
				videos = new HashMap<String, Integer>();
				sunburst = new Sunburst("Week "+week, new ArrayList<SunburstNode>());
			}
			String action = data.getPayload().split("\":\"")[0].substring(2);
			String str = data.getPayload().split("\":\"")[1];
			String videoId = str.substring(0, str.length()-2);
			if (action.equals("LOADED")) {
				if (!titles.containsKey(videoId))
					titles.put(videoId, YouTubeData.getTitleById(videoId));
				if (!videos.containsKey(videoId))
					videos.put(videoId, 1);
				else
					videos.put(videoId, videos.get(videoId)+1);
			}
		}

		Sunburst chart = new Sunburst("Embedded Video", content);
		return chart;
	}

}
