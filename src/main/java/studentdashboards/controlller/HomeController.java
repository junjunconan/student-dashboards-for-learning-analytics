package studentdashboards.controlller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import studentdashboards.model.Data2D;
import studentdashboards.model.PieChartModel;
import studentdashboards.service.DataDao;

@Controller
@Scope("session")
public class HomeController {

	@Autowired
	private DataDao dataDao;

	@RequestMapping(value = "/")
	public String home(HttpSession session) {
		String user = (String) session.getAttribute("user");
		if (user == null)
			return "index";
		else if (user.equals("admin"))
			return "instructor";
		else if (user != null && user != "")
			return "student";
		return "index";
	}

	@RequestMapping(value = "/summary/events")
	@ResponseBody
	public List<Data2D<Long, Integer>> summaryEvents(HttpSession session) {
		String user = (String) session.getAttribute("user");
		List<Data2D<Date, Integer>> result = null;
		if (user == null)
			return null;
		else if (user.equals("admin"))
			result = dataDao.getSummaryByEvents();
		else
			result = dataDao.getSummaryByEvents(user);
		List<Data2D<Long, Integer>> values = new ArrayList<Data2D<Long, Integer>>();
		for (Data2D<Date, Integer> data : result) {
			Data2D<Long, Integer> value = new Data2D<Long, Integer>(data.getX().getTime(), data.getY());
			values.add(value);
		}
		return values;
	}

	@RequestMapping(value = "/summary/num-of-users")
	@ResponseBody
	public List<Data2D<Long, Integer>> summaryNumOfUsers() {
		List<Data2D<Date, Integer>> result = dataDao.getSummaryByNumOfUsers();
		List<Data2D<Long, Integer>> values = new ArrayList<Data2D<Long, Integer>>();
		for (Data2D<Date, Integer> data : result) {
			Data2D<Long, Integer> value = new Data2D<Long, Integer>(data.getX().getTime(), data.getY());
			values.add(value);
		}
		return values;
	}

	@RequestMapping(value = "/summary/weekday")
	@ResponseBody
	public List<PieChartModel<Integer>> summaryWeekday(HttpSession session) {
		String user = (String) session.getAttribute("user");
		List<Data2D<Date, Integer>> result = null;
		if (user == null)
			return null;
		else if (user.equals("admin"))
			result = dataDao.getSummaryByWeekday();
		else
			result = dataDao.getSummaryByWeekday(user);

		DateFormat dateFormat=new SimpleDateFormat("EE", Locale.US);
		List<PieChartModel<Integer>> values = new ArrayList<PieChartModel<Integer>>();
		for (Data2D<Date, Integer> data : result) {
			PieChartModel<Integer> value = new PieChartModel<Integer>(dateFormat.format(data.getX()), data.getY());
			values.add(value);
		}
		return values;
	}

	@RequestMapping(value = "/summary/time")
	@ResponseBody
	public List<PieChartModel<Integer>> summaryTime(HttpSession session) {
		String user = (String) session.getAttribute("user");
		List<Data2D<Date, Integer>> result = null;
		if (user == null)
			return null;
		else if (user.equals("admin"))
			result = dataDao.getSummaryByTime();
		else
			result = dataDao.getSummaryByTime(user);

		List<PieChartModel<Integer>> values = new ArrayList<PieChartModel<Integer>>();
		int morning = 0, afternoon = 0, evening = 0, night = 0;
		for (Data2D<Date, Integer> data : result) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(data.getX());
			int hour = calendar.get(Calendar.HOUR_OF_DAY);
			if (hour >= 0 && hour < 6)
				night += data.getY();
			else if (hour >= 6 && hour < 12)
				morning += data.getY();
			else if (hour >=12 && hour < 18)
				afternoon += data.getY();
			else
				evening += data.getY();
		}
		values.add(new PieChartModel<Integer>("Morning", morning));
		values.add(new PieChartModel<Integer>("Afternoon", afternoon));
		values.add(new PieChartModel<Integer>("Evening", evening));
		values.add(new PieChartModel<Integer>("Night", night));
		return values;
	}

}
