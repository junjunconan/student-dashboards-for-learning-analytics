package studentdashboards;

import javax.sql.DataSource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;

import studentdashboards.service.DataDao;
import studentdashboards.service.DataDaoImpl;

@SpringBootApplication
public class Application {

	@Bean
	public JdbcTemplate jdbcTemplate(DataSource dataSource) {
		return new JdbcTemplate(dataSource);
	}

	@Bean
	public DataDao dataDao() {
		return new DataDaoImpl();
	}

	public @interface Exclude {};

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
