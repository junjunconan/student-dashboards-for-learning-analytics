package studentdashboards.util;

import java.io.IOException;
import java.util.List;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.Video;
import com.google.api.services.youtube.model.VideoListResponse;
import com.google.api.services.youtube.model.VideoSnippet;

public class YouTubeData {

	private static final String API_KEY = "AIzaSyBAgjXMQ7x9fut-Pvws_k0-xUd12hXYyQ8";
	private static final String APPLICATION_NAME = "junjunconan";
	private static final String PART = "snippet";
	private static final String FIELDS = "items(id,snippet(title))";

	public static String getTitleById(String id) throws Exception {
		YouTube youtube = new YouTube.Builder(new NetHttpTransport(), new JacksonFactory(), new HttpRequestInitializer() {
			public void initialize(HttpRequest request) throws IOException {
			}
		}).setApplicationName(APPLICATION_NAME).build();

		YouTube.Videos.List videos = youtube.videos().list("id,snippet");
		videos.setKey(API_KEY);
		videos.setPart(PART);
		videos.setFields(FIELDS);
		videos.setId(id);
		VideoListResponse response = videos.execute();
		List<Video> list = response.getItems();
		String title = null;
		for (Video video : list) {
			VideoSnippet snippet = (VideoSnippet) video.get("snippet");
			title = snippet.getTitle();
		}

		return title;
	}
}
