package studentdashboards;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import studentdashboards.model.ImportData;

public class DataItemProcessor implements ItemProcessor<ImportData, ImportData> {

	private static final Logger log = LoggerFactory.getLogger(DataItemProcessor.class);

	public ImportData process(final ImportData data) throws Exception {

		log.info(data.toString());

		return data;
	}

}
