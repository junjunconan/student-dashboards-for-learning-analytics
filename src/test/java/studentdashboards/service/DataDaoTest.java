package studentdashboards.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;

import java.util.Date;
import java.util.List;

import org.junit.Test;

import studentdashboards.TestBase;
import studentdashboards.model.Data;
import studentdashboards.model.Data2D;
import studentdashboards.model.Data3D;

public class DataDaoTest extends TestBase {

	@Test
	public void getByUserTest() throws Exception {
		List<Data> result = dataDao.getByUser(USER_ID);
		assertNotNull(result);
		assertNotSame(result.size(), 0);
	}

	@Test
	public void getByUserTestWrongUserId() throws Exception {
		List<Data> result = dataDao.getByUser(WRONG_USER_ID);
		assertNotNull(result);
		assertSame(result.size(), 0);
	}

	@Test
	public void getSummaryByEventsTest() throws Exception {
		List<Data2D<Date, Integer>> result = dataDao.getSummaryByEvents();
		assertNotNull(result);
		assertNotSame(result.size(), 0);

		result = dataDao.getSummaryByEvents(USER_ID);
		assertNotNull(result);
		assertNotSame(result.size(), 0);
	}

	@Test
	public void getSummaryByNumOfUserTest() throws Exception {
		List<Data2D<Date, Integer>> result = dataDao.getSummaryByNumOfUsers();
		assertNotNull(result);
		assertNotSame(result.size(), 0);
	}

	@Test
	public void getSummaryByWeekdayTest() throws Exception {
		List<Data2D<Date, Integer>> result = dataDao.getSummaryByWeekday();
		assertNotNull(result);
		assertNotSame(result.size(), 0);

		result = dataDao.getSummaryByWeekday(USER_ID);
		assertNotNull(result);
		assertNotSame(result.size(), 0);
	}

	@Test
	public void getSummaryByTimeTest() throws Exception {
		List<Data2D<Date, Integer>> result = dataDao.getSummaryByTime();
		assertNotNull(result);
		assertNotSame(result.size(), 0);

		result = dataDao.getSummaryByTime(USER_ID);
		assertNotNull(result);
		assertNotSame(result.size(), 0);
	}

	@Test
	public void getDetailByActionTest() throws Exception {
		List<Data3D<String, Date, Integer>> result = dataDao.getDetailByAction();
		assertNotNull(result);
		assertNotSame(result.size(), 0);

		result = dataDao.getDetailByAction(USER_ID);
		assertNotNull(result);
		assertNotSame(result.size(), 0);

		result = dataDao.getDetailByAction(WEEK_NUMBER);
		assertNotNull(result);
		assertNotSame(result.size(), 0);

		result = dataDao.getDetailByAction(USER_ID, WEEK_NUMBER);
		assertNotNull(result);
		assertNotSame(result.size(), 0);
	}

	@Test
	public void getExcoTest() throws Exception {
		List<Data> result = dataDao.getExco();
		assertNotNull(result);
		assertNotSame(result.size(), 0);

		result = dataDao.getExco(USER_ID);
		assertNotNull(result);
		assertNotSame(result.size(), 0);
	}

	@Test
	public void getEmbeddedVideoTest() throws Exception {
		List<Data> result = dataDao.getEmbeddedVideo();
		assertNotNull(result);
		assertNotSame(result.size(), 0);

		result = dataDao.getEmbeddedVideo(USER_ID);
		assertNotNull(result);
		assertNotSame(result.size(), 0);
	}

}
