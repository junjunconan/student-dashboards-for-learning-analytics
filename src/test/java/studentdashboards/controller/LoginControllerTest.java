package studentdashboards.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;

import studentdashboards.ControllerTestBase;

public class LoginControllerTest extends ControllerTestBase {

	@Test
	public void testLogin() throws Exception {
		mockMvc.perform(post("/login").param("userid", USER_ID))
		.andExpect(status().is3xxRedirection())
		.andExpect(request().sessionAttribute("user", USER_ID))
		.andExpect(redirectedUrl("/"));
	}

	@Test
	public void testLoginVerifyInstructor() throws Exception {
		mockMvc.perform(post("/login/verify").param("userid", INSTRUCTOR_ID))
		.andExpect(status().isOk())
		.andExpect(content().string(VERIFY_SUCCESS));
	}

	@Test
	public void testLoginVerifyRegularUser() throws Exception {
		mockMvc.perform(post("/login/verify").param("userid", USER_ID))
		.andExpect(status().isOk())
		.andExpect(content().string(VERIFY_SUCCESS));
	}

	@Test
	public void testLoginVerifyWrongUser() throws Exception {
		mockMvc.perform(post("/login/verify").param("userid", WRONG_USER_ID))
		.andExpect(status().isOk())
		.andExpect(content().string(VERIFY_FAIL));
	}

	@Test
	public void testLogout() throws Exception {
		mockMvc.perform(get("/logout"))
		.andExpect(status().is3xxRedirection())
		.andExpect(redirectedUrl("/"));
	}

}
