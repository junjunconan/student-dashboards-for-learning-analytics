package studentdashboards.controller;

import static org.hamcrest.CoreMatchers.both;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.nio.charset.Charset;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import studentdashboards.ControllerTestBase;

public class HomeControllerTest extends ControllerTestBase {

	@Test
	public void testHomePage() throws Exception {
		mockMvc.perform(get("/"))
		.andExpect(status().isOk())
		.andExpect(view().name("index"));
	}

	@Test
	public void testInstructorHomePage() throws Exception {
		mockMvc.perform(get("/").sessionAttr("user", INSTRUCTOR_ID))
		.andExpect(status().isOk())
		.andExpect(view().name("instructor"));
	}

	@Test
	public void testStudentHomePage() throws Exception {
		mockMvc.perform(get("/").sessionAttr("user", USER_ID))
		.andExpect(status().isOk())
		.andExpect(view().name("student"));
	}

	@Test
	public void testSummaryByEvents() throws Exception {
		MvcResult result = mockMvc.perform(get("/summary/events").sessionAttr("user", INSTRUCTOR_ID))
				.andExpect(status().isOk())
				.andExpect(content().contentType(new MediaType(
						MediaType.APPLICATION_JSON.getType(),
						MediaType.APPLICATION_JSON.getSubtype(),
						Charset.forName("utf8")
						)))
				.andReturn();
		String response = result.getResponse().getContentAsString();
		assertThat(response, both(containsString("\"x\"")).and(containsString("\"y\"")));
	}

	@Test
	public void testSummaryByUserEvents() throws Exception {
		MvcResult result = mockMvc.perform(get("/summary/events").sessionAttr("user", USER_ID))
				.andExpect(status().isOk())
				.andExpect(content().contentType(new MediaType(
						MediaType.APPLICATION_JSON.getType(),
						MediaType.APPLICATION_JSON.getSubtype(),
						Charset.forName("utf8")
						)))
				.andReturn();
		String response = result.getResponse().getContentAsString();
		assertThat(response, both(containsString("\"x\"")).and(containsString("\"y\"")));
	}

	@Test
	public void testSummaryByNumOfUsers() throws Exception {
		MvcResult result = mockMvc.perform(get("/summary/num-of-users"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(new MediaType(
						MediaType.APPLICATION_JSON.getType(),
						MediaType.APPLICATION_JSON.getSubtype(),
						Charset.forName("utf8")
						)))
				.andReturn();
		String response = result.getResponse().getContentAsString();
		assertThat(response, both(containsString("\"x\"")).and(containsString("\"y\"")));
	}

	@Test
	public void testSummaryByWeekday() throws Exception {
		MvcResult result = mockMvc.perform(get("/summary/weekday").sessionAttr("user", INSTRUCTOR_ID))
				.andExpect(status().isOk())
				.andExpect(content().contentType(new MediaType(
						MediaType.APPLICATION_JSON.getType(),
						MediaType.APPLICATION_JSON.getSubtype(),
						Charset.forName("utf8")
						)))
				.andReturn();
		String response = result.getResponse().getContentAsString();
		assertThat(response, both(containsString("\"label\"")).and(containsString("\"value\"")));
	}

	@Test
	public void testSummaryByWeekdayUser() throws Exception {
		MvcResult result = mockMvc.perform(get("/summary/weekday").sessionAttr("user", USER_ID))
				.andExpect(status().isOk())
				.andExpect(content().contentType(new MediaType(
						MediaType.APPLICATION_JSON.getType(),
						MediaType.APPLICATION_JSON.getSubtype(),
						Charset.forName("utf8")
						)))
				.andReturn();
		String response = result.getResponse().getContentAsString();
		assertThat(response, both(containsString("\"label\"")).and(containsString("\"value\"")));
	}

	@Test
	public void testSummaryByTime() throws Exception {
		MvcResult result = mockMvc.perform(get("/summary/time").sessionAttr("user", INSTRUCTOR_ID))
				.andExpect(status().isOk())
				.andExpect(content().contentType(new MediaType(
						MediaType.APPLICATION_JSON.getType(),
						MediaType.APPLICATION_JSON.getSubtype(),
						Charset.forName("utf8")
						)))
				.andReturn();
		String response = result.getResponse().getContentAsString();
		assertThat(response, both(containsString("\"label\"")).and(containsString("\"value\"")));
	}

	@Test
	public void testSummaryByTimeUser() throws Exception {
		MvcResult result = mockMvc.perform(get("/summary/time").sessionAttr("user", USER_ID))
				.andExpect(status().isOk())
				.andExpect(content().contentType(new MediaType(
						MediaType.APPLICATION_JSON.getType(),
						MediaType.APPLICATION_JSON.getSubtype(),
						Charset.forName("utf8")
						)))
				.andReturn();
		String response = result.getResponse().getContentAsString();
		assertThat(response, both(containsString("\"label\"")).and(containsString("\"value\"")));
	}

}
