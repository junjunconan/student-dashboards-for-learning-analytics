package studentdashboards.controller;

import static org.hamcrest.CoreMatchers.both;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.nio.charset.Charset;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import studentdashboards.ControllerTestBase;

public class DetailControllerTest extends ControllerTestBase {

	@Test
	public void testDetailPage() throws Exception {
		mockMvc.perform(get("/detail"))
		.andExpect(status().is3xxRedirection())
		.andExpect(redirectedUrl("/"));

		mockMvc.perform(post("/detail").sessionAttr("user", INSTRUCTOR_ID))
		.andExpect(status().isOk())
		.andExpect(view().name("detail"));
	}

	@Test
	public void testDetailByAction() throws Exception {
		MvcResult result = mockMvc.perform(get("/detail/actions/all").sessionAttr("user", INSTRUCTOR_ID))
				.andExpect(status().isOk())
				.andExpect(content().contentType(new MediaType(
						MediaType.APPLICATION_JSON.getType(),
						MediaType.APPLICATION_JSON.getSubtype(),
						Charset.forName("utf8")
						)))
				.andReturn();
		String response = result.getResponse().getContentAsString();
		assertThat(response, both(containsString("\"key\"")).and(containsString("\"values\"")));
	}

	@Test
	public void testDetailByUserAction() throws Exception {
		MvcResult result = mockMvc.perform(get("/detail/actions/all").sessionAttr("user", USER_ID))
				.andExpect(status().isOk())
				.andExpect(content().contentType(new MediaType(
						MediaType.APPLICATION_JSON.getType(),
						MediaType.APPLICATION_JSON.getSubtype(),
						Charset.forName("utf8")
						)))
				.andReturn();
		String response = result.getResponse().getContentAsString();
		assertThat(response, both(containsString("\"key\"")).and(containsString("\"values\"")));
	}

	@Test
	public void testDetailByWeekAction() throws Exception {
		MvcResult result = mockMvc.perform(get("/detail/actions/"+WEEK_NUMBER).sessionAttr("user", INSTRUCTOR_ID))
				.andExpect(status().isOk())
				.andExpect(content().contentType(new MediaType(
						MediaType.APPLICATION_JSON.getType(),
						MediaType.APPLICATION_JSON.getSubtype(),
						Charset.forName("utf8")
						)))
				.andReturn();
		String response = result.getResponse().getContentAsString();
		assertThat(response, both(containsString("\"key\"")).and(containsString("\"values\"")));
	}

	@Test
	public void testDetailByUserWeekAction() throws Exception {
		MvcResult result = mockMvc.perform(get("/detail/actions/"+WEEK_NUMBER).sessionAttr("user", USER_ID))
				.andExpect(status().isOk())
				.andExpect(content().contentType(new MediaType(
						MediaType.APPLICATION_JSON.getType(),
						MediaType.APPLICATION_JSON.getSubtype(),
						Charset.forName("utf8")
						)))
				.andReturn();
		String response = result.getResponse().getContentAsString();
		assertThat(response, both(containsString("\"key\"")).and(containsString("\"values\"")));
	}

}
