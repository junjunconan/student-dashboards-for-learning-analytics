package studentdashboards.controller;

import static org.hamcrest.CoreMatchers.both;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.nio.charset.Charset;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import studentdashboards.ControllerTestBase;

public class EmbeddedVideoControllerTest extends ControllerTestBase {

	@Test
	public void testEmbeddedVideoPage() throws Exception {
		mockMvc.perform(get("/embedded-video"))
		.andExpect(status().is3xxRedirection())
		.andExpect(redirectedUrl("/"));

		mockMvc.perform(post("/embedded-video").sessionAttr("user", INSTRUCTOR_ID))
		.andExpect(status().isOk())
		.andExpect(view().name("video"));
	}

	@Test
	public void testGetEmbeddedVideo() throws Exception {
		MvcResult result = mockMvc.perform(get("/embedded-video/get").sessionAttr("user", INSTRUCTOR_ID))
				.andExpect(status().isOk())
				.andExpect(content().contentType(new MediaType(
						MediaType.APPLICATION_JSON.getType(),
						MediaType.APPLICATION_JSON.getSubtype(),
						Charset.forName("utf8")
						)))
				.andReturn();
		String response = result.getResponse().getContentAsString();
		assertThat(response, both(containsString("\"name\"")).and(containsString("\"children\"")));
	}

	@Test
	public void testGetEmbeddedVideoByUser() throws Exception {
		MvcResult result = mockMvc.perform(get("/embedded-video/get").sessionAttr("user", USER_ID))
				.andExpect(status().isOk())
				.andExpect(content().contentType(new MediaType(
						MediaType.APPLICATION_JSON.getType(),
						MediaType.APPLICATION_JSON.getSubtype(),
						Charset.forName("utf8")
						)))
				.andReturn();
		String response = result.getResponse().getContentAsString();
		assertThat(response, both(containsString("\"name\"")).and(containsString("\"children\"")));
	}

}
