package studentdashboards.model;

import static org.junit.Assert.assertSame;

import org.junit.Test;

import studentdashboards.TestBase;

public class Data2DTest extends TestBase {

	@Test
	public void testData2DModel() throws Exception {
		Data2D<String, Integer> data = new Data2D<String, Integer>();
		data.setX(ID);
		data.setY(TEST_NUMBER);
		assertSame(data.getX(), ID);
		assertSame(data.getY(), TEST_NUMBER);
	}

}
