package studentdashboards.model;

import static org.junit.Assert.assertSame;

import org.junit.Test;

import studentdashboards.TestBase;

public class ImportDataTest extends TestBase {

	@Test
	public void testImportDataModel() throws Exception {
		ImportData data = new ImportData(ID, RECEIVED, CONTEXT, USER_ID,
				USER_ID, PAYLOAD, REMOTE_ADDR, WEEK, TOPIC);
		data.setActionId(ACTION_ID);
		assertSame(data.getId(), ID);
		assertSame(data.getReceived(), RECEIVED);
		assertSame(data.getUserId(), USER_ID);
		assertSame(data.getActionId(), ACTION_ID);
		assertSame(data.getContext(), CONTEXT);
		assertSame(data.getPayload(), PAYLOAD);
		assertSame(data.getRemoteAddr(), REMOTE_ADDR);
		assertSame(data.getWeek(), WEEK);
		assertSame(data.getTopic(), TOPIC);
	}

}
