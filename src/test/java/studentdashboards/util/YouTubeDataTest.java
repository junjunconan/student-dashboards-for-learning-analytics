package studentdashboards.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import studentdashboards.TestBase;

public class YouTubeDataTest extends TestBase {

	@Test
	public void getTitleByIdTest() throws Exception {
		String title = YouTubeData.getTitleById(YOUTUBE_ID);
		assertEquals(title, YOUTUBE_TITLE);
	}

}
