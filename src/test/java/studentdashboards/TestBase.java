package studentdashboards;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import studentdashboards.service.DataDao;

@RunWith(SpringJUnit4ClassRunner.class)
@Application.Exclude
@WebAppConfiguration
@SpringApplicationConfiguration(classes=Application.class)
@Ignore
public class TestBase {

	protected static final String ID = "1268";
	protected static final String RECEIVED = "2014-07-25 15:03:29";
	protected static final String CONTEXT = "elec1601";
	protected static final String USER_ID = "248";
	protected static final int WEEK_NUMBER = 3;
	protected static final String WRONG_USER_ID = "123456";
	protected static final String INSTRUCTOR_ID = "admin";
	protected static final String ACTION_ID = "resource-view";
	protected static final String PAYLOAD = "{\"url\":\"https://flip.ee.usyd.edu.au/elec1601/\"}";
	protected static final String REMOTE_ADDR = "129.78.233.211";
	protected static final String WEEK = "0";
	protected static final String TOPIC = "-";
	protected static final int TEST_NUMBER = 100;
	protected static final String VERIFY_SUCCESS = "{ \"valid\" : true }";
	protected static final String VERIFY_FAIL = "{ \"valid\" : false }";
	protected static final String YOUTUBE_ID = "SeoW3YNo3Zs";
	protected static final String YOUTUBE_TITLE = "Encoding integers in sign and magnitude and 2s complement";

	@Autowired
	protected DataDao dataDao;

}
