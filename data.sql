create database if not exists studentdashboards;

use studentdashboards;

drop table if exists data;

create table data (
  id		int(11) primary key,
  received  datetime,
  context	varchar(255),
  user_id   varchar(30),
  action_id	varchar(255),
  payload   varchar(5000),
  remote_addr	varchar(30),
  week		int(11), 
  topic     varchar(255)
);